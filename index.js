const express = require('express');
const app = express();
const PORT = 8080;

app.get('/', (req, res) => {
    res.send('<h1>Hello</h1>');
});

app.listen(PORT, () => {
    console.log('server is up and running');
});